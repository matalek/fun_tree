/* FunTree
 * Aleksander Matusiak (am347171)
 *
 * Przyjęte założenia:
 *
 * - typ zwracany przez fun<T> jest funkcją z T w T
 *
 * - podobnie wyniki operatorów dla FunTree<T> są również typu T
 *
 * - typy komparatora, predykatu, operacji binarnych i unarnych
 * nie zmieniają przyjmowanych argumentów, zatem jednocześnie drzewo
 * może zmieniać się tylko w wyniku wykonania funkcji insert i erase
 *
 * - dla każdego typu drzewa FunTree<T> pamiętany jest osobny porządek
 * wypisywania
 */

#ifndef FUN_TREE_H
#define FUN_TREE_H

#include <assert.h>
#include <functional>
#include <memory>
#include <ostream>

template <typename T>
class FunTree {

    public:

        // Udostępniane typy
        using Comparator = std::function<bool(const T&, const T&)>;
        using Operator = std::function<void(const T&)>;
        using UnaryOperator = std::function<T(const T&)>;
        using BinaryOperator = std::function<T(const T&, const T&)>;
        using Predicate = std::function<bool(const T&)>;

        // Konstrukcja i przypisywanie
        FunTree() = default;

        FunTree(const FunTree& other) {
            node = (other.node == nullptr) ? nullptr : std::make_shared<Node>(*(other.node));
        }

        FunTree& operator=(const FunTree& other) {
            node = (other.node == nullptr) ? nullptr : std::make_shared<Node>(*(other.node));
            return *this;
        }

    private:

        // Pomocnicza klasa reprezentujące węzeł drzewa
        class Node {

            public:
            
                Node(T val) : val(val), left(nullptr), right(nullptr) { }

                Node(const Node& other) {
                    val = other.val;
                    left = (other.left == nullptr) ? nullptr : std::make_shared<Node>(*(other.left));
                    right = (other.right == nullptr) ? nullptr : std::make_shared<Node>(*(other.right));
                }

                /* Wstawia element zgodnie z podanym komparatorem
                 * do drzewa zaczepionego w danym węźle */
                void insert(const T& el, const Comparator& comp) {
                    if (comp(el, val)) {
                        if (left == nullptr)
                            left = std::make_shared<Node>(el);
                        else
                            left->insert(el, comp);
                    } else {
                        if (right == nullptr)
                            right = std::make_shared<Node>(el);
                        else
                            right->insert(el, comp);
                    }
                }

                /* Sprawdza zgodnie z podanym komparatorem, czy dany
                 * element należy do drzewa zaczepionego w danym węźle */
                bool find(const T& el, const Comparator& comp) const {
                    if (comp(el, val)) {
                        if (left == nullptr)
                            return false;
                        else
                            return left->find(el, comp);
                    } else if (comp(val, el)) {
                        if (right == nullptr)
                            return false;
                        else
                            return right->find(el, comp);
                    } else
                        return true;
                }

                /* Usuwa, szukając zgodnie z podanym komparatorem, węzeł
                 * o danej wartości z całym zaczepionym poddrzewem.
                 * Zwraca true wtedy i tylko wtedy, gdy dany wartość
                 * jest wartością danego węzła, żeby przodek mógł usunąć
                 * ten węzeł */
                bool erase(const T& el, const Comparator& comp) {
                    if (comp(el, val)) {
                        if (left != nullptr && left->erase(el, comp))
                            left = nullptr;
                        return false;
                    } else if (comp(val, el)) {
                        if (right != nullptr && right->erase(el, comp))
                            right = nullptr;
                        return false;
                    } else
                        return true;
                }
            
                // Wartość w węźle
                T val;
                // Wskaźniki na synów
                std::shared_ptr<Node> left;
                std::shared_ptr<Node> right;
        };

        // Wskaźnik na węzeł
        std::shared_ptr<Node> node;
        
        // Pomocniczy typ reprezentujący porządek przechodzenia
        using Traversal = std::function<void(const Node&, const Operator&)>;
        
    public:
        
        /* Wstawia kopię elementu do drzewa, używając podanego komparatora do
         * porównywania elementów. Nie zwraca żadnej wartości. */
        void insert(const T& el, const Comparator& comp = std::less<T>()) {
            if (node == nullptr)
                node = std::make_shared<Node>(el);
            else
                node->insert(el, comp);
        }

        /* Sprawdza, czy drzewo zawiera dany element, używając podanego
         * komparatora do porównywania elementów. Zwraca wartość typu bool. */
        bool find(const T& el, const Comparator& comp = std::less<T>()) const {
            if (node == nullptr)
                return false;
            else
                return node->find(el, comp);
        }

        /* Jeśli, używając podanego komparatora do porównywania elemementów,
         * znajdzie jakiś element równy podanemu, to usuwa go z drzewa wraz
         * z całym jego podrzewem. W p.p. nic nie robi. Nie zwraca żadnej
         * wartości. */
        void erase(const T& el, const Comparator& comp = std::less<T>()) {
            if (node != nullptr)
                if (node->erase(el, comp))
                    // jeśli wyszukiwany element był w korzeniu drzewa
                    node = nullptr; 
        }

        /* Aplikuje funkcję operation do wszystkich elementów danego drzewa
         * zgodnie z porządkiem określonym przez traversal. Nie modyfikuje
         * elementów przechowywanych w drzewie. */
        void apply(const Operator& op, const Traversal& order = inorder) const {
            if (node != nullptr)
                order(*node, op);
        }

        /* Tworzy nowe drzewo na podstawie istniejącego, zawierające
         * wszystkie elementy po transformacji tj. po zaaplikowaniu funkcji
         * unary_operation. Przegląda drzewo w porządku traversal. Do nowego
         * drzewa wstawia elementy, używając komparatora comparator. */
        FunTree<T> map(const UnaryOperator& op, const Traversal& order = inorder, const Comparator& comp = std::less<T>()) const {
            FunTree<T> res;
            if (node != nullptr)
                order(*node, [&](const T& val)->void {
                    res.insert(op(val), comp);
                });
            return res;
        }

        /* Tworzy nowe drzewo na podstawie istniejącego, zawierające jedynie
         * elementy, które spełniają predykat predicate. Przegląda drzewo
         * w porządku traversal. Do nowego drzewa wstawia elementy, używając
         * komparatora comparator. */
        FunTree<T> filter(const Predicate& pr, const Traversal& order = inorder, const Comparator& comp = std::less<T>()) const {
            FunTree<T> res;
            if (node != nullptr)
                order(*node, [&](const T& val)->void {
                    if (pr(val))
                        res.insert(val, comp);
                });
            return res;
        }

        /* Oblicza i zwraca wynik akumulacji elementów drzewa za pomocą
         * dwuargumentowej funkcji binary_operation. Jej pierwszym parametrem
         * jest akumulowana wartość. Jej drugim parametrem jest wartość
         * przechowywana w węźle drzewa. Parametr init jest początkową
         * wartością wyniku. Przegląda drzewo w porządku traversal */
        T fold(const BinaryOperator& op, T init, const Traversal& order = inorder) const {
            if (node != nullptr)
                order(*node, [&](const T& val)->void {
                    init = op(init, val);
                });
            return init;
        }

        /* Parametr f jest funkcją jednoargumentową. Parametr op jest
         * operatorem dwuargumentowym. Zwraca funkcję g(n) = f(n) op root,
         * gdzie root jest wartością elementu przechowywanego w korzeniu
         * drzewa. */
        UnaryOperator fun(const UnaryOperator& f, const BinaryOperator& op) const {
            // niezdefiniowane zachowanie, gdy drzewo jest puste
            assert(node != nullptr);
            
            /* przekazujemy wartość w korzeniu przez wartość, gdyż
             * (zgodnie z odpowiedzią na forum), wartość w korzeniu może
             * się zmienić przed wywołaniem wyniku fun */
            T value = node->val;
            return [&, value](T n)->T {
                return op(f(n), value);
            };
        }

        /* Funkcje reprezentujące porządki przechodzenia, które wykonują
         * rekurencyjnie operator na elementach drzewa, zgodnie
         * z reprezentowanym porządkiem. */
        
        static const Traversal inorder;
        static const Traversal preorder;
        static const Traversal postorder;
        
        // Aktualnie ustawiony porządek dla danego typu drzew
        static Traversal ostream_order;

        // Ustawia porządek wypisywania
        friend ::std::ostream& operator<<(::std::ostream& os, const Traversal& new_order) {
            ostream_order = new_order;
            return os;
        }
        
        // Wypisuje drzewo zgodnie z ustalonym porządkiem.
        friend std::ostream& operator<<(std::ostream& os, const FunTree& ft) {
            ft.apply([&](const T& val)->void {
                os << " " << val;
            }, FunTree<T>::ostream_order);
            return os;
        }
        
};

// Początkowo ustawionym porządkiem dla wszystkich typów jest porządek infiksowy.
template <typename T>
typename FunTree<T>::Traversal FunTree<T>::ostream_order = FunTree<T>::inorder;

// Reprezentuje porządek przechodzenie infiksowy.
template<typename T>
typename FunTree<T>::Traversal const FunTree<T>::inorder =
    [](const FunTree<T>::Node& tree_node, const FunTree<T>::Operator& fun) {

    if (tree_node.left != nullptr)
        inorder(*tree_node.left, fun);
    fun(tree_node.val);
    if (tree_node.right != nullptr)
        inorder(*tree_node.right, fun);
};

// Reprezentuje porządek przechodzenie prefiksowy.
template <typename T>
typename FunTree<T>::Traversal const FunTree<T>::preorder =
    [](const FunTree<T>::Node& tree_node, const FunTree<T>::Operator& fun) {

    fun(tree_node.val);
    if (tree_node.left != nullptr)
        preorder(*tree_node.left, fun);
    if (tree_node.right != nullptr)
        preorder(*tree_node.right, fun);
};

// Reprezentuje porządek przechodzenie postfiksowy.
template <typename T>
typename FunTree<T>::Traversal const FunTree<T>::postorder =
    [](const FunTree<T>::Node& tree_node, const FunTree<T>::Operator& fun) {

    if (tree_node.left != nullptr)
        postorder(*tree_node.left, fun);
    if (tree_node.right != nullptr)
        postorder(*tree_node.right, fun);
    fun(tree_node.val);
};

#endif // FUN_TREE_H
