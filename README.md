## Zadanie 7. ##
### Języki i narzędzia programowania I ###
### Wydział Matematyki, Informatyki i Mechaniki Uniwersytetu Warszawskiego ###

=== Zadanie zabawne drzewa binarne ===

Zadanie polega na zaimplementowaniu klasy szablonowej FunTree, która
reprezentuje zabawne drzewa binarne.

Klasa FunTree powinna mieć bezparametrowy konstruktor tworzący puste
drzewo oraz standardową semantykę kopiowania i przenoszenia.

W poniższych opisach comparator to dwuargumentowa funkcja zwracająca
wartość bool. Komparatora używa się przy przeszukiwaniu drzewa.
Jeśli komparator zwróci true, należy przejść do lewego potomka,
a w p.p. -- do prawego. Jeśli komparator nie został podany, to jego
domyślną wartością jest std::less.

W poniższych opisach traversal to funkcja reprezentująca porządek
przeglądania całego drzewa. Jeśli jej nie podano, to domyślnym
porządkiem jest porządek infiksowy.

Drzewo powinno dostarczać standardowych operacji:

* insert(element, comparator)
    Wstawia kopię elementu do drzewa, używając podanego komparatora do
    porównywania elementów. Nie zwraca żadnej wartości.

* find(element, comparator)
    Sprawdza, czy drzewo zawiera dany element, używając podanego
    komparatora do porównywania elementów. Zwraca wartość typu bool.

* erase(element, comparator)
    Jeśli, używając podanego komparatora do porównywania elemementów,
    znajdzie jakiś element równy podanemu, to usuwa go z drzewa wraz
    z całym jego podrzewem. W p.p. nic nie robi. Nie zwraca żadnej
    wartości.

Pomocnicze funkcje statyczne reprezentujące porządek przeglądania
całego drzewa:

* inorder
    Reprezentuje porządek przechodzenie infiksowy.

* preorder
    Reprezentuje porządek przechodzenie prefiksowy.

* postorder
     Reprezentuje porządek przechodzenie postfiksowy.

Wypisywanie drzewa:

* operator<<
    Wypisuje elementy drzewa zgodnie z ustawionym porządkiem.
    Początkowo ustawionym porządkiem jest porządek infiksowy.
    Porządek wypisywania można zmieniać, wysyłając funkcję go
    reprezentującą do operatora.

Oprócz powyższych, klasa powinna dostarczać też operacji:

* apply(operation, traversal)
    Aplikuje funkcję operation do wszystkich elementów danego drzewa
    zgodnie z porządkiem określonym przez traversal. Nie modyfikuje
    elementów przechowywanych w drzewie.

* map(unary_operation, traversal, comparator)
    Tworzy nowe drzewo na podstawie istniejącego, zawierające
    wszystkie elementy po transformacji tj. po zaaplikowaniu funkcji
    unary_operation. Przegląda drzewo w porządku traversal. Do nowego
    drzewa wstawia elementy, używając komparatora comparator.

* filter(predicate, traversal, comparator)
    Tworzy nowe drzewo na podstawie istniejącego, zawierające jedynie
    elementy, które spełniają predykat predicate. Przegląda drzewo
    w porządku traversal. Do nowego drzewa wstawia elementy, używając
    komparatora comparator.

* fold(binary_operation, init, traversal)
    Oblicza i zwraca wynik akumulacji elementów drzewa za pomocą
    dwuargumentowej funkcji binary_operation. Jej pierwszym parametrem
    jest akumulowana wartość. Jej drugim parametrem jest wartość
    przechowywana w węźle drzewa. Parametr init jest początkową
    wartością wyniku. Przegląda drzewo w porządku traversal

* fun(f, op)
    Parametr f jest funkcją jednoargumentową. Parametr op jest
    operatorem dwuargumentowym. Zwraca funkcję
        g(n) = f(n) op root,
    gdzie root jest wartością elementu przechowywanego w korzeniu
    drzewa.

Uwaga końcowa: parametrem szablonowym klasy może być funkcja, a także
szablon funkcji.

== Przykładowe użycie ==

```
#include <iostream>
#include "fun_tree.h"

int main() {
    FunTree<int> bst_tree;
    bst_tree.insert(3);
    bst_tree.insert(5);
    bst_tree.insert(4);
    bst_tree.insert(2);
    bst_tree.insert(1);
    ::std::cout << "bst_tree" << ::std::endl
                << FunTree<int>::inorder << bst_tree << ::std::endl
                << FunTree<int>::preorder << bst_tree << ::std::endl
                << FunTree<int>::postorder << bst_tree << ::std::endl;

    FunTree<int>::Comparator fun_comparator =
        [](int a, int b)->int {
            static int i = 0b0111010;
            i >>= 1;
            return i & 1;
        };
    FunTree<int> fun_tree;
    fun_tree.insert(1, fun_comparator);
    fun_tree.insert(2, fun_comparator);
    fun_tree.insert(3, fun_comparator);
    fun_tree.insert(4, fun_comparator);
    fun_tree.insert(5, fun_comparator);
    ::std::cout << "fun_tree" << ::std::endl
                << FunTree<int>::inorder << fun_tree << ::std::endl
                << FunTree<int>::preorder << fun_tree << ::std::endl
                << FunTree<int>::postorder << fun_tree << ::std::endl;

    ::std::cout << "fun_tree.find(3) = " << fun_tree.find(3) << ::std::endl;
    ::std::cout << "fun_tree.find(5) = " << fun_tree.find(5) << ::std::endl;

    FunTree<int>::Operator fun_printer = [](int e) {
        ::std::cout << e << '$';
    };
    ::std::cout << "custom print:";
    bst_tree.apply(fun_printer);
    ::std::cout << ::std::endl;

    FunTree<int>::UnaryOperator fun_un_op = [](int e)->int {
        return e - 2;
    };
    FunTree<int> minus_two = bst_tree.map(fun_un_op,
                                          FunTree<int>::preorder,
                                          ::std::greater<int>{});
    ::std::cout << "minus two:"
                << FunTree<int>::inorder << minus_two << ::std::endl;

    FunTree<int>::Predicate fun_predicate = [](int e)->bool {
        return (e & 1) == 1;
    };
    FunTree<int> odd = minus_two.filter(fun_predicate);
    ::std::cout << "odd:" << odd << ::std::endl;

    FunTree<int>::BinaryOperator fun_bin_op = ::std::plus<int>{};
    int sum = bst_tree.fold(fun_bin_op, 0, FunTree<int>::inorder);
    ::std::cout << "sum: " << sum << ::std::endl;

    fun_tree.erase(2, ::std::greater<int>{});
    ::std::cout << "fun_tree:" << fun_tree << ::std::endl;

    FunTree<int> tmp_tree(bst_tree);
    ::std::cout << "tmp_tree:" << tmp_tree << ::std::endl;
    fun_tree = tmp_tree;
    ::std::cout << "fun_tree:" << fun_tree << ::std::endl;

    FunTree<int>::UnaryOperator g = fun_tree.fun(fun_un_op, fun_bin_op);
    ::std::cout << "g(7) = " << g(7) << ::std::endl;
}
```

== Wynik działania przykładu ==

```
bst_tree
 1 2 3 4 5
 3 2 1 5 4
 1 2 4 5 3
fun_tree
 4 2 5 1 3
 1 2 4 5 3
 4 5 2 3 1
fun_tree.find(3) = 1
fun_tree.find(5) = 0
custom print:1$2$3$4$5$
minus two: 3 2 1 0 -1
odd: -1 1 3
sum: 15
fun_tree: 1 3
tmp_tree: 1 2 3 4 5
fun_tree: 1 2 3 4 5
g(7) = 8
```

== Rozwiązanie ==

Rozwiązanie powinno składać się z jednego pliku: fun_tree.h.
Plik ten należy umieścić w repozytorium w katalogu

grupaN/zadanie7/ab123456

gdzie N jest numerem grupy, a ab123456 identyfikatorem rozwiązującego.
Katalog z rozwiązaniem nie powinien zawierać innych plików, ale może
zawierać podkatalog private, gdzie można umieszczać różne pliki, np.
swoje testy. Pliki umieszczone w tym podkatalogu nie będą oceniane.